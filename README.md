# Webworker

Diese Repository enthält ein simples Beispiel eines Webworkers und wie dieser mit dem Mainthread kommunizieren kann.

## Installation und Ausführen

Ein lokaler Webserver für das Repository kann mit folgenden Befehlen gestartet werden. Danach im Browser
zu [http://localhost:8080](http://localhost:8080) navigieren.

`> npm install`

`> gulp`