self.addEventListener("message", function (event) {
    switch (event.data.cmd) {
        case "hi":
            self.postMessage({
                cmd: "print",
                data: "How's it going Main Thread? Here is your worker!"
            });
            break;

        case "iterate":
            self.postMessage({
                cmd: "print",
                data: "Worker going to iterate."
            });

            var count = 0;
            for (var i = 0; i < 1000000000; i++) {
                count++;
            }

            self.postMessage({
                cmd: "print",
                data: "Worker done."
            });
            break;
    }
});