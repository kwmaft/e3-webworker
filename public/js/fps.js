$(function () {
    var
        $fps = $(".js-fps-display"),
        getFPS = (function () {
            var lastLoop = (new Date()).getMilliseconds();
            var count = 1;
            var fps = 0;

            return function () {
                var currentLoop = (new Date()).getMilliseconds();
                if (lastLoop > currentLoop) {
                    fps = count;
                    count = 1;
                } else {
                    count += 1;
                }
                lastLoop = currentLoop;
                console.log("counted fps");
                return fps;
            };
        })();

    (function loop() {
        requestAnimationFrame(function () {
            $fps.html(getFPS());
            loop();
        });
    })();
});