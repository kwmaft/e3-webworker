$(function () {
    var Console = {
        $el: $(".js-console-content"),

        start: function () {
            this.worker = new Worker("./js/worker.js");

            this.worker.addEventListener("message", function (event) {
                Console.commands[event.data.cmd](event.data.data);
            });

            $(".js-console-command").on("click", function () {
                Console.commands[$(this).data("command")]();
            });
        },

        commands: {
            print: function (message) {
                Console.$el.html(Console.$el.html() + message + "\n");
            },

            hi: function () {
                Console.worker.postMessage({
                    cmd: "hi"
                });
            },

            main: function () {
                Console.commands.print("Main Thread going to iterate.");

                var count = 0;
                for (var i = 0; i < 1000000000; i++) {
                    count++;
                }

                Console.commands.print("Main Thread done.");
            },

            worker: function () {
                Console.worker.postMessage({
                    cmd: "iterate"
                });
            }
        }
    };

    Console.start();
});