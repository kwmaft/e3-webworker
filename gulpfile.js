const
    gulp = require("gulp"),
    connect = require("gulp-connect");

gulp.task("jquery", function () {
    return gulp
        .src("node_modules/jquery/dist/jquery.min.js")
        .pipe(gulp.dest("public/js"));
});

gulp.task("connect", function () {
    connect.server({
        root: "public",
        livereload: true
    });
});

gulp.task("default", gulp.series("jquery", "connect"));